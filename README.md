# Darker Boxy sheme for Hubzilla

**English**

Darker-Boxy is a color scheme for the official Hubzilla redbasic theme. The files are to be put in /view/theme/redbasic/schema by a admin.

To activate it on a channel, users will have to go to /settings/display of their hub and select it in the "Set color palette" menu. If he/she wants, in the personal settings of the theme, he/she can set the value 64 in the field "Set maximum width of content region in rem".

**Français**

Darker-Boxy est un schéma de couleurs pour le thème officiel Hubzilla redbasic. Les fichiers sont à mettre dans /view/theme/redbasic/schema par un admin.

Pour l'activer sur un canal, les utilisateurs devront se rendre à /settings/display de leur hub et le sélectionner dans le menu "Définir la palette de couleurs". S'il le souhaite, dans les paramètres personnels du thème, il/elle pourra mettre la valeur 64 dans le champs "Set maximum width of content region in rem"

## Languages used :
PHP and CSS

## Some screenshots :
<img title="TheChangebook" src="https://i.imgur.com/bwh7X6R.png">

<img title="TheChangebook" src="https://i.imgur.com/Msa7Kbm.png">

<img title="TheChangebook" src="https://i.imgur.com/HWVgB31.png">

Go here to see more : [https://hub.thechangebook.org/channel/dandandauge](https://hub.thechangebook.org/channel/dandandauge)

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dandauge/darker-boxy-sheme-for-hubzilla.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/dandauge/darker-boxy-sheme-for-hubzilla/-/settings/integrations)

